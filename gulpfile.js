var gulp = require('gulp');
var uglify = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');
var concat = require('gulp-concat');
var sass = require('gulp-ruby-sass');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();
var stripCssComments = require('gulp-strip-css-comments');


// **********************************
// Obsługa błędów
// **********************************
function errorLog (error) {
    console.error.bind(error);
    this.emit('end');
}

// **********************************
// Live Reload via 'BrowserSync'
// **********************************
gulp.task('serve',['sass','scripts','styles'],function() {
    console.log('████████████████████████████████████████████████████████████████████████');
    console.log('████████████████████████ S T A R T -  L I V E  S R V. ██████████████████');
    console.log('████████████████████████████████████████████████████████████████████████');
    browserSync.init({
        server: "./",
        notify: true,
        // proxy: "http://localhost/pinion/"
    });

    gulp.watch(['assets/js/**/*.js','!assets/js/**/*.min.js'],['scripts']);
    gulp.watch(['assets/scss/**/*.scss'],['sass']);
    gulp.watch(['assets/css/**/*.css',"!assets/css/**/*.min.css"],['styles']);
    gulp.watch(['*.html']).on('change', browserSync.reload);
});



// **********************************
// Minifijacja JS
// Command line: gulp scripts
// **********************************
gulp.task('scripts',function(){
    console.log('▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ Make all in one for JS');
    gulp.src('assets/js/**/*.js')
//        .pipe(sourcemaps.init())                        // inicjujemy mapę
//        .pipe(uglify())                                 // kompresujemy
//        .pipe(concat('scripts.min.js'))                 // pakujemy do jednego
//        .pipe(sourcemaps.write('.'))                    // generujemy mapę
//        .pipe(gulp.dest('assets/js'))                   // ścieżka zapisu
        .pipe(browserSync.reload({stream: true}));        // auto reload
});



// **********************************
// Compile SASS
// Command line: gulp saas
// **********************************
gulp.task('sass',function(){
    console.log('▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ Compile SASS');
    return sass('assets/scss/**/*.scss',{
//      sourcemap: false,                               // przygotuj mapy      [true,fale]
        stopOnError: true,                              // przerwij jeśli błąd
        style:'compressed'                              // skompresuj zawartość  [nested, compact, compressed, expanded]
    })                                                  // koniec kompilacji
        .on('error', errorLog)                          // zgłasza błędy do konsoli
/*
        .pipe(sourcemaps.write('.', {                   // inicjujemy mapę
            includeContent: false,                      // inicjujemy mapę
            sourceRoot: 'source'                        // inicjujemy mapę
        }))
*/
        .pipe(stripCssComments({preserve: false}))      // kastruje komentarze  [false, true] w zaleznosci czy m zostawiać wazne  /*! ... */
        .pipe(gulp.dest('assets/css'))                  // ścieżka zapisu
        .pipe(browserSync.stream({match:'**/*.css'}));  // auto reload
});



// **********************************
// Minifijacja CSS
// Command line: gulp styles
// **********************************
gulp.task('styles',function(){
    console.log('▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ Make all in one copmress CSS');
    gulp.src('assets/css/*.css')
/*
        .pipe(concat('all.min.css'))                    // pakujemy do jednego
        .pipe(uglifycss({                               // kompresujemy
            "maxLineLen": 80,                           // kompresujemy
            "uglyComments": true                        // kompresujemy
        }))                                             // kompresujemy
*/
        .pipe(gulp.dest('assets/css'))                  // ścieżka zapisu
});



// **********************************
// Watch Task
// Command line: gulp watch
// **********************************
gulp.task('watch',function(){
    console.log('████████████████████████████████████████████████████████████████████████');
    console.log('████████████████████████ S T A R T -  W A T C H ████████████████████████');
    console.log('████████████████████████████████████████████████████████████████████████');

//    gulp.watch(['assets/js/**/*.js','!assets/js/**/*.min.js'],['scripts']);
    gulp.watch('assets/scss/**/*.scss',['sass']);
//    gulp.watch(['assets/css/**/*.css',"!assets/css/**/*.min.css"],['styles']);
});







// **********************************
// Default Task
// **********************************
// gulp.task('default',['scripts','sass','styles','serve']);
gulp.task('default',['scripts','sass','styles','serve']);
